<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_admin extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('m_admin');
	}

	public function index(){
		$data['admin'] = $this->m_admin->getAllUsers();
		$this->load->view('admin/admin', $data);
		$this->load->view('footer');
	}

	public function addnew(){
		$this->load->view('admin/baru');
	}

	public function insert(){
		$user['id_pegawai'] = $this->input->post('id_pegawai');
		$user['nama_pegawai'] = $this->input->post('nama_pegawai');
		$user['password'] = $this->input->post('password');

		$query = $this->m_admin->insertuser($user);

		// if ($query) {
		// 	header('location:'.base_url().$this->index());
		// }

		$data['admin'] = $this->m_admin->getAllUsers();
		$this->load->view('admin/admin', $data);
		$this->load->view('footer');
	}

	public function edit($id){
		$data['admin'] = $this->m_admin->getUser($id);
		$this->load->view('admin/edit',$data);
	}

	public function update($id){
		$user['id_pegawai'] = $this->input->post('id_pegawai');
		$user['nama_pegawai'] = $this->input->post('nama_pegawai');
		$user['password'] = $this->input->post('password');

		$query = $this->m_admin->updateuser($user, $id);

		// if ($query) {
		// 	header('location:'.base_url().$this->index());
		// }

		$data['admin'] = $this->m_admin->getAllUsers();
		$this->load->view('admin/admin', $data);
		$this->load->view('footer');
	}

	public function delete($id)
	{
		$query = $this->m_admin->deleteuser($id);

		// if ($query) {
		// 	header('location:'.base_url().$this->index());
		// }

		$data['admin'] = $this->m_admin->getAllUsers();
		$this->load->view('admin/admin', $data);
		$this->load->view('footer');
	}

}
?>