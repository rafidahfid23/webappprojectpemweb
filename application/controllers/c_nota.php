<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_nota extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('m_nota');
	}

	public function index(){
		$data['nota'] = $this->m_nota->getAllUsers();
		$this->load->view('nota/nota', $data);
		$this->load->view('footer');
	}

	public function addnew(){
		$this->load->view('nota/baru');
	}

	public function insert(){
		$user['id_nota'] = $this->input->post('id_nota');
		$user['nama_pelanggan'] = $this->input->post('nama_pelanggan');
		$user['nama_pegawai'] = $this->input->post('nama_pegawai');
		$user['tanggal'] = $this->input->post('tanggal');
		$user['nama_bunga'] = $this->input->post('nama_bunga');
		$user['jumlah'] = $this->input->post('jumlah');
		$user['harga'] = $this->input->post('harga');

		$query = $this->m_nota->insertuser($user);

		// if ($query) {
		// 	header('location:'.base_url().$this->index());
		// }

		$data['nota'] = $this->m_nota->getAllUsers();
		$this->load->view('nota/nota', $data);
		$this->load->view('footer');
	}

	public function edit($id){
		$data['nota'] = $this->m_nota->getUser($id);
		$this->load->view('nota/edit',$data);
		
	}

	public function update($id){
		$user['id_nota'] = $this->input->post('id_nota');
		$user['nama_pelanggan'] = $this->input->post('nama_pelanggan');
		$user['nama_pegawai'] = $this->input->post('nama_pegawai');
		$user['tanggal'] = $this->input->post('tanggal');
		$user['nama_bunga'] = $this->input->post('nama_bunga');
		$user['jumlah'] = $this->input->post('jumlah');
		$user['harga'] = $this->input->post('harga');

		$query = $this->m_nota->updateuser($user, $id);

		// if ($query) {
		// 	header('location:'.base_url().$this->index());
		// }

		$data['nota'] = $this->m_nota->getAllUsers();
		$this->load->view('nota/nota', $data);
		$this->load->view('footer');
	}

	public function delete($id)
	{
		$query = $this->m_nota->deleteuser($id);

		// if ($query) {
		// 	header('location:'.base_url().$this->index());
		// }

		$data['nota'] = $this->m_nota->getAllUsers();
		$this->load->view('nota/nota', $data);
		$this->load->view('footer');
	}

}
?>