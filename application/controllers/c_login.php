<?php

class c_login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('m_bunga');
    }

    public function index()
    {
        $this->load->view("login");
    }

    public function proses_login(){
       $this->form_validation->set_rules('id_pegawai', 'id_pegawai', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        if ($this->form_validation->run() == TRUE) {
            $this->load->model('m_bunga');
            $data=$this->m_bunga->cek_login();
            if($data->num_rows()>0){
                $id_pegawai=$data->row();
                $array = array(
                    'login'=>TRUE,
                    'id_pegawai' => $id_pegawai->id_pegawai,
                    'password'   => $id_pegawai->password
                );
                
                $this->session->set_userdata( $array );
                redirect('c_bunga/index','refresh');
            }
            else {
                $this->session->set_flashdata('pesan', 'id_pegawai dan password salah');
                redirect('c_login/index','refresh');
            }
        } 
        else {
            $this->session->set_flashdata('pesan', validation_errors());
            redirect('c_login/index','refresh');
        }
    }
    public function logout()
    {
        $this->session->sess_destroy();
        redirect('c_login/index','refresh');
    }
}
?>