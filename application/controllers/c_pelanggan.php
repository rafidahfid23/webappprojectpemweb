<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_pelanggan extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('m_pelanggan');
	}

	public function index(){
		$data['pelanggan'] = $this->m_pelanggan->getAllUsers();
		$this->load->view('pelanggan/pelanggan', $data);
		$this->load->view('footer');
	}

	public function addnew(){
		$this->load->view('pelanggan/baru');
	}

	public function insert(){
		$user['id_pelanggan'] = $this->input->post('id_pelanggan');
		$user['nama_pelanggan'] = $this->input->post('nama_pelanggan');
		$user['alamat'] = $this->input->post('alamat');
		$user['no_telpon'] = $this->input->post('no_telpon');

		$query = $this->m_pelanggan->insertuser($user);

		// if ($query) {
		// 	header('location:'.base_url().$this->index());
		// }

		$data['pelanggan'] = $this->m_pelanggan->getAllUsers();
		$this->load->view('pelanggan/pelanggan', $data);
		$this->load->view('footer');
	}

	public function edit($id){
		$data['pelanggan'] = $this->m_pelanggan->getUser($id);
		$this->load->view('pelanggan/edit',$data);
	}

	public function update($id){
		$user['id_pelanggan'] = $this->input->post('id_pelanggan');
		$user['nama_pelanggan'] = $this->input->post('nama_pelanggan');
		$user['alamat'] = $this->input->post('alamat');
		$user['no_telpon'] = $this->input->post('no_telpon');

		$query = $this->m_pelanggan->updateuser($user, $id);

		// if ($query) {
		// 	header('location:'.base_url().$this->index());
		// }

		$data['pelanggan'] = $this->m_pelanggan->getAllUsers();
		$this->load->view('pelanggan/pelanggan', $data);
		$this->load->view('footer');
	}

	public function delete($id)
	{
		$query = $this->m_pelanggan->deleteuser($id);

		// if ($query) {
		// 	header('location:'.base_url().$this->index());
		// }

		$data['pelanggan'] = $this->m_pelanggan->getAllUsers();
		$this->load->view('pelanggan/pelanggan', $data);
		$this->load->view('footer');
	}


}
?>