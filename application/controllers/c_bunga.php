<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_bunga extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('m_bunga');
	}

	public function index(){
		$data['bunga'] = $this->m_bunga->getAllUsers();
		$this->load->view('bunga/bunga', $data);
		$this->load->view('footer');
	}

	public function addnew(){
		$this->load->view('bunga/baru');
	}

	public function insert(){
		$user['id_bunga'] = $this->input->post('id_bunga');
		$user['nama_bunga'] = $this->input->post('nama_bunga');
		$user['stok'] = $this->input->post('stok');
		$user['harga'] = $this->input->post('harga');

		$query = $this->m_bunga->insertuser($user);

		// if ($query) {
		// 	header('location:'.base_url().$this->index());
		// }
		
		$data['bunga'] = $this->m_bunga->getAllUsers();
		$this->load->view('bunga/bunga', $data);
		$this->load->view('footer');
	}

	public function edit($id){
		$data['bunga'] = $this->m_bunga->getUser($id);
		$this->load->view('bunga/edit',$data);
	}

	public function update($id){
		$user['id_bunga'] = $this->input->post('id_bunga');
		$user['nama_bunga'] = $this->input->post('nama_bunga');
		$user['stok'] = $this->input->post('stok');
		$user['harga'] = $this->input->post('harga');

		$query = $this->m_bunga->updateuser($user, $id);

		// if ($query) {
		// 	header('location:'.base_url().$this->index());
		// }

		$data['bunga'] = $this->m_bunga->getAllUsers();
		$this->load->view('bunga/bunga', $data);
		$this->load->view('footer');
	}

	public function delete($id)
	{
		$query = $this->m_bunga->deleteuser($id);

		// if ($query) {
		// 	header('location:'.base_url().$this->index());
		// }

		$data['bunga'] = $this->m_bunga->getAllUsers();
		$this->load->view('bunga/bunga', $data);
		$this->load->view('footer');
	}

}
?>