<?php
	class m_bunga extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->database();
		}

		public function cek_login()
		{
		return $this->db->where('id_pegawai', $this->input->post('id_pegawai'))
						->where('password', $this->input->post('password'))
						->get  ('admin');
		}

		public function getAllUsers(){
			$query = $this->db->get('bunga');
			return $query->result();
		}

		public function insertuser($user)
		{
			return $this->db->insert('bunga', $user);
		}

		public function getUser($id)
		{
			$query = $this->db->get_where('bunga', array('id_bunga'=>$id));
			return $query->row_array();
		}

		public function updateuser($user, $id)
		{
			$this->db->where('bunga.id_bunga', $id);
			return $this->db->update('bunga', $user);
		}

		public function deleteuser($id)
		{
			$this->db->where('bunga.id_bunga', $id);
			return $this->db->delete('bunga');
		}
	}
?>