<?php
	class m_nota extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->database();
		}

		public function getAllUsers(){
			$query = $this->db->get('nota');
			return $query->result();
		}

		public function insertuser($user)
		{
			return $this->db->insert('nota', $user);
		}

		public function getUser($id)
		{
			$query = $this->db->get_where('nota', array('id_nota'=>$id));
			return $query->row_array();
		}

		public function updateuser($user, $id)
		{
			$this->db->where('nota.id_nota', $id);
			return $this->db->update('nota', $user);
		}

		public function deleteuser($id)
		{
			$this->db->where('nota.id_nota', $id);
			return $this->db->delete('nota');
		}
	}
?>