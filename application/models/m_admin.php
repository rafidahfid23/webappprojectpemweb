<?php
	class m_admin extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->database();
		}


		public function getAllUsers(){
			$query = $this->db->get('admin');
			return $query->result();
		}

		public function insertuser($user)
		{
			return $this->db->insert('admin', $user);
		}

		public function getUser($id)
		{
			$query = $this->db->get_where('admin', array('id_pegawai'=>$id));
			return $query->row_array();
		}

		public function updateuser($user, $id)
		{
			$this->db->where('admin.id_pegawai', $id);
			return $this->db->update('admin', $user);
		}

		public function deleteuser($id)
		{
			$this->db->where('admin.id_pegawai', $id);
			return $this->db->delete('admin');
		}
	}
?>