<?php
	class m_pelanggan extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->database();
		}


		public function getAllUsers(){
			$query = $this->db->get('pelanggan');
			return $query->result();
		}

		public function insertuser($user)
		{
			return $this->db->insert('pelanggan', $user);
		}

		public function getUser($id)
		{
			$query = $this->db->get_where('pelanggan', array('id_pelanggan'=>$id));
			return $query->row_array();
		}

		public function updateuser($user, $id)
		{
			$this->db->where('pelanggan.id_pelanggan', $id);
			return $this->db->update('pelanggan', $user);
		}

		public function deleteuser($id)
		{
			$this->db->where('pelanggan.id_pelanggan', $id);
			return $this->db->delete('pelanggan');
		}
	}
?>