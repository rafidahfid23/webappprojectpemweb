<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html, charset=utf-8">
	<title>Edit Nota | Toko Bunga NOFM</title>
	<link rel="icon" type="image/png" href="<?php echo base_url() ?>asset/admin/img/nofm.png"/>
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/css/bootstrap.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script type="text/javascript" src="<?= base_url();?>asset/js/jquery-3.2.1.js"></script>
	
	<script type="text/javascript" src="<?=base_url();?>asset/js/bootstrap.js"></script>
</head>
<body style="background-image: url('<?php echo base_url() ?>asset/admin/img/bghijau2.png');">
<!-- Navbar -->
<nav class="navbar navbar-expand navbar-dark bg-dark">
		<a class="nav-link" href="<?php echo base_url(); ?>index.php/c_nota/index">
			<img src="<?php echo base_url();?>asset/admin/img/back-white.png" alt="" width="25">
		</a>
		<a class="navbar-brand" href="<?php echo base_url(); ?>index.php/c_nota/index" style="font-size: 30px;" >Toko Bunga NOFM</a>
  		<!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    	<span class="navbar-toggler-icon"></span>
  		</button> -->
	</nav>
    <!-- End Navbar -->
<div class="container"style="margin-top: 40px; margin-left: 200px;">
	<div class="row">
		<div class="col-sm-6 col-sm-offset-4" style="margin-left: 170px;">
			<h3 style="color: #22222D;">Edit Data Nota</h3>
			<hr>
			<?php extract($nota);?>
			<form method="POST" action="<?php echo base_url(); ?>index.php/c_nota/update/<?php echo $id_nota; ?>">
				<div class="form-group">
					<label style="color: #22222D;">ID Nota</label>
					<input type="text" class="form-control" value="<?php echo $id_nota;?>" name="id_nota">
				</div>
				<div class="form-group">
					<label style="color: #22222D;">Nama Pelanggan</label>
					<input type="text" class="form-control" value="<?php echo $nama_pelanggan;?>" name="nama_pelanggan">
				</div>
				<div class="form-group">
					<label style="color: #22222D;">Nama Pegawai</label>
					<input type="text" class="form-control" value="<?php echo $nama_pegawai;?>" name="nama_pegawai">
				</div>
				<div class="form-group">
					<label style="color: #22222D;">Tangga:</label>
					<input type="date" class="form-control" value="<?php echo $tanggal;?>" name="tanggal">
				</div>
				<div class="form-group">
					<label style="color: #22222D;">Nama Bunga</label>
					<input type="text" class="form-control" value="<?php echo $nama_bunga;?>" name="nama_bunga">
				</div>
				<div class="form-group">
					<label style="color: #22222D;">Jumlah</label>
					<input type="text" class="form-control" value="<?php echo $jumlah;?>" name="jumlah">
				</div>
				<div class="form-group">
					<label style="color: #22222D;">Harga</label>
					<input type="text" class="form-control" value="<?php echo $harga;?>" name="harga"><br>
				</div>
				<button type="submit" class="btn btn-success" style="margin-top: -10px; margin-bottom: 15px;"><span class="glyphicon glyphicon-check"></span> Update</button>
			</form>
		</div>
	</div>
</div>
</body>
</html>