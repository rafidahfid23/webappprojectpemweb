<!DOCTYPE html>
<html lang="en">

<head>
  <title>Nota | Toko Bunga NOFM</title>
  <link rel="icon" type="image/png" href="<?php echo base_url() ?>asset/admin/img/nofm.png"/>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta content="widtd=device-widtd, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- Material Kit CSS -->
  <link href="<?php echo base_url();?>asset/admin/css/material-dashboard.css?v=2.1.0" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/bootstrap.css">
</head>

<body class="dark-edition" style="background-color: #22222D;">
  <div class="wrapper ">
    <div class="sidebar" data-color="orange" data-background-color="black" data-image="<?php echo base_url();?>asset/admin/img/sidebar-3.jpg" >
      <!--
      Tip 1: You can change tde color of tde sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
      <div class="logo">
      <a href="<?php echo base_url();?>index.php/c_bunga/index" class="simple-text logo-normal">
          <img src="<?php echo base_url();?>asset/admin/img/nofm.png" alt="" width="50%">
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>index.php/c_bunga/index">
              <i class="material-icons">local_florist</i>
              <p>Bunga</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>index.php/c_pelanggan/index">
              <i class="material-icons">account_circle</i>
              <p>Pelanggan</p>
            </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo base_url();?>index.php/c_nota/index">
              <i class="material-icons">class</i>
              <p>Nota</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url();?>index.php/c_admin/index">
              <i class="material-icons">face</i>
              <p>Admin</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url();?>index.php/c_login/logout">
              <i class="material-icons">exit_to_app</i>
              <p>Logout</p>
            </a>
          </li>
          <!-- your sidebar here -->
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-dark bg-dark" style="border-radius: 0px;">
        <span class="navbar-brand mb-0 h1" style="font-size: 30px; color: #9C9CA1;">Toko Bunga NOFM</span>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid" style="margin: auto;">
          <div class="container" style="margin-top: -50px;">
          <p style="font-size: 30px; color:#9C9CA1; margin-left: -3px; font-weight:400; color: #9C9CA1">Daftar Nota</p>
    <div class="row">
      <div class="col-sm-12 col-sm-offset-2">
        <a href="<?php echo base_url(); ?>index.php/c_nota/addnew" class="btn" style="background-color: #EA700D; margin-left: 172px; margin-top: -75px;">
          <span class="glyphicon glyphicon-plus"></span> + Add New
        </a>
        <br>
        <br>
        <table class="table table-striped table-dark rounded" style="background-color:#2A293C; text-align:center; margin-left:-3px; margin-top: -20px;">
          <thead>
            <tr>
              <th scope="col" style="font-weight: 400;">ID Nota</th>
              <th scope="col" style="font-weight: 400;">Nama Pelanggan</th>
              <th scope="col" style="font-weight: 400;">Nama Pegawai</th>
              <th scope="col" style="font-weight: 400;">Tanggal</th>
              <th scope="col" style="font-weight: 400;">Nama Bunga</th>
              <th scope="col" style="font-weight: 400;">Jumlah</th>
              <th scope="col" style="font-weight: 400;">Harga</th>
              <th scope="col" style="font-weight: 400;">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach ($nota as $user) {
              ?>
              <tr>
                <td><?php echo $user->id_nota; ?></td>
                <td><?php echo $user->nama_pelanggan; ?></td>
                <td><?php echo $user->nama_pegawai; ?></td>
                <td><?php echo $user->tanggal; ?></td>
                <td><?php echo $user->nama_bunga; ?></td>
                <td><?php echo $user->jumlah; ?></td>
                <td><?php echo $user->harga; ?></td>
                <td>
                  <a href="<?php echo base_url(); ?>index.php/c_nota/edit/<?php echo $user->id_nota; ?>" class="btn btn-success">
                    <span class="glyphicon glyphicon-edit"></span> Edit
                  </a> ||
                  <a href="<?php echo base_url(); ?>index.php/c_nota/delete/<?php echo $user->id_nota; ?>" class="btn btn-danger">
                    <span class="glyphicon glyphicon-trash"></span> Delete
                  </a>
                </td>
              </tr>
              <?php
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
        </div>
      </div>      
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="<?php echo base_url();?>asset/admin/js/core/jquery.min.js"></script>
  <script src="<?php echo base_url();?>asset/admin/js/core/popper.min.js"></script>
  <script src="<?php echo base_url();?>asset/admin/js/core/bootstrap-material-design.min.js"></script>
  <script src="https://unpkg.com/default-passive-events"></script>
  <script src="<?php echo base_url();?>asset/admin/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Place tdis tag in your head or just before your close body tag. -->
  <script async defer src="https://buttons.gitdub.io/buttons.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->
  <script src="<?php echo base_url();?>asset/admin/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="<?php echo base_url();?>asset/admin/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for tde example pages etc -->
  <script src="<?php echo base_url();?>asset/admin/js/material-dashboard.js?v=2.1.0"></script>
  <!-- Material Dashboard DEMO metdods, don't include it in your project! -->
  <script src="<?php echo base_url();?>asset/admin/demo/demo.js"></script>
  <script>
    $(document).ready(function() {
      $().ready(function() {
        $sidebar = $('.sidebar');

        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');

        window_widtd = $(window).widtd();

        $('.fixed-plugin a').click(function(event) {
          // Alex if we click on switch, stop propagation of tde event, so tde dropdown will not be hide, otderwise we set tde  section active
          if ($(tdis).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else if (window.event) {
              window.event.cancelBubble = true;
            }
          }
        });

        $('.fixed-plugin .active-color span').click(function() {
          $full_page_background = $('.full-page-background');

          $(tdis).siblings().removeClass('active');
          $(tdis).addClass('active');

          var new_color = $(tdis).data('color');

          if ($sidebar.lengtd != 0) {
            $sidebar.attr('data-color', new_color);
          }

          if ($full_page.lengtd != 0) {
            $full_page.attr('filter-color', new_color);
          }

          if ($sidebar_responsive.lengtd != 0) {
            $sidebar_responsive.attr('data-color', new_color);
          }
        });

        $('.fixed-plugin .background-color .badge').click(function() {
          $(tdis).siblings().removeClass('active');
          $(tdis).addClass('active');

          var new_color = $(tdis).data('background-color');

          if ($sidebar.lengtd != 0) {
            $sidebar.attr('data-background-color', new_color);
          }
        });

        $('.fixed-plugin .img-holder').click(function() {
          $full_page_background = $('.full-page-background');

          $(tdis).parent('li').siblings().removeClass('active');
          $(tdis).parent('li').addClass('active');


          var new_image = $(tdis).find("img").attr('src');

          if ($sidebar_img_container.lengtd != 0 && $('.switch-sidebar-image input:checked').lengtd != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $sidebar_img_container.fadeIn('fast');
            });
          }

          if ($full_page_background.lengtd != 0 && $('.switch-sidebar-image input:checked').lengtd != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function() {
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
              $full_page_background.fadeIn('fast');
            });
          }

          if ($('.switch-sidebar-image input:checked').lengtd == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
          }

          if ($sidebar_responsive.lengtd != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
          }
        });

        $('.switch-sidebar-image input').change(function() {
          $full_page_background = $('.full-page-background');

          $input = $(tdis);

          if ($input.is(':checked')) {
            if ($sidebar_img_container.lengtd != 0) {
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.lengtd != 0) {
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image', '#');
            }

            background_image = true;
          } else {
            if ($sidebar_img_container.lengtd != 0) {
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.lengtd != 0) {
              $full_page.removeAttr('data-image', '#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').change(function() {
          $body = $('body');

          $input = $(tdis);

          if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

          } else {

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

            setTimeout(function() {
              $('body').addClass('sidebar-mini');

              md.misc.sidebar_mini_active = true;
            }, 300);
          }

          // we simulate tde window Resize so tde charts will get updated in realtime.
          var simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop tde simulation of Window Resize after tde animations are completed
          setTimeout(function() {
            clearInterval(simulateWindowResize);
          }, 1000);

        });
      });
    });
  </script>
</body>

</html>