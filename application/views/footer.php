<footer class="footer">
        <div class="container-fluid">
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, Made With <i class="material-icons">favorite</i> by NOFM
          </div>
          <!-- your footer here -->
        </div>
      </footer>