<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html, charset=utf-8">
	<title>Tambah Bunga | Toko Bunga NOFM</title>
	<link rel="icon" type="image/png" href="<?php echo base_url() ?>asset/admin/img/nofm.png"/>
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/css/bootstrap.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script type="text/javascript" src="<?= base_url();?>asset/js/jquery-3.2.1.js"></script>
	
	<script type="text/javascript" src="<?=base_url();?>asset/js/bootstrap.js"></script>
</head>
<body style="background-image: url('<?php echo base_url() ?>asset/admin/img/bgorange1.png');">
	<!-- Navbar -->
	<nav class="navbar navbar-expand navbar-dark bg-dark">
		<a class="nav-link" href="<?php echo base_url(); ?>index.php/c_bunga/index">
			<img src="<?php echo base_url();?>asset/admin/img/back-white.png" alt="" width="25">
		</a>
		<a class="navbar-brand" href="<?php echo base_url(); ?>index.php/c_bunga/index" style="font-size: 30px;" >Toko Bunga NOFM</a>
  		<!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    	<span class="navbar-toggler-icon"></span>
  		</button> -->
	</nav>
    <!-- End Navbar -->

<div class="container" style="margin-top: 40px; margin-left: 220px;">
	<div class="row">
		<div class="col-sm-6 col-sm-offset-4" style="margin-left: 150px;">
			<h3 style="color: #22222D;">Tambah Data Bunga</h3>
			<hr>
			<form method="POST" action="<?php echo base_url(); ?>index.php/c_bunga/insert">
				<div class="form-group">
					<label style="color: #22222D;">ID Bunga</label>
					<input type="text" class="form-control" name="id_bunga">
				</div>
				<div class="form-group">
					<label style="color: #22222D;">Nama Bunga</label>
					<input type="text" class="form-control" name="nama_bunga">
				</div>
				<div class="form-group">
					<label style="color: #22222D;">Stok</label>
					<input type="text" class="form-control" name="stok">
				</div>
				<div class="form-group">
					<label style="color: #22222D;">Harga</label>
					<input type="text" class="form-control" name="harga">
				</div>
				<button type="submit" class="btn" style="background-color: #EA700D; width: 100px; font-weight:bold"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
			</form>
		</div>
	</div>
</div>
</body>
</html>